package hu.bme.aut.android.todo

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.DialogFragment
import hu.bme.aut.android.todo.databinding.FragmentEditBinding
import hu.bme.aut.android.todo.model.DatePickerDialogFragment
import hu.bme.aut.android.todo.model.Todo

class TodoEditFragment(val originalTodo: Todo) : DialogFragment(), DatePickerDialogFragment.DateListener  {
    private lateinit var listener: TodoEditedListener
    private lateinit var binding: FragmentEditBinding

    override fun onAttach(context: Context) {
        super.onAttach(context)

        try {
            listener = if (targetFragment != null) {
                targetFragment as TodoEditedListener
            } else {
                activity as TodoEditedListener
            }
        } catch (e: ClassCastException) {
            throw RuntimeException(e)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentEditBinding.inflate(inflater, container, false)
        dialog?.setTitle(R.string.itemEditTodo)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.spnrTodoPriority.adapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_item,
            listOf("Low", "Medium", "High")
        )

        binding.spnrTodoPriority.setSelection( when (originalTodo.priority) {
            Todo.Priority.LOW -> 0
            Todo.Priority.MEDIUM -> 1
            Todo.Priority.HIGH -> 2
        })

        binding.etTodoTitle.setText(originalTodo.title)

        binding.tvTodoDueDate.text = originalTodo.dueDate

        binding.tvTodoDueDate.setOnClickListener {
            showDatePickerDialog()
        }

        binding.etTodoDescription.setText(originalTodo.description)

        binding.btnEditTodo.setOnClickListener {
            val selectedPriority = when (binding.spnrTodoPriority.selectedItemPosition) {
                0 -> Todo.Priority.LOW
                1 -> Todo.Priority.MEDIUM
                2 -> Todo.Priority.HIGH
                else -> Todo.Priority.LOW
            }

            listener.onTodoEdited(
                Todo(
                id = originalTodo.id,
                title = binding.etTodoTitle.text.toString(),
                priority = selectedPriority,
                dueDate = binding.tvTodoDueDate.text.toString(),
                description = binding.etTodoDescription.text.toString()
            )
            )
            dismiss()
        }

        binding.btnCancelEditTodo.setOnClickListener {
            dismiss()
        }

    }

    private fun showDatePickerDialog() {
        val datePicker = DatePickerDialogFragment()
        datePicker.setTargetFragment(this, 0)
        datePicker.show(fragmentManager!!, DatePickerDialogFragment.TAG)
    }

    override fun onDateSelected(date: String) {
        binding.tvTodoDueDate.text = date
    }

    interface TodoEditedListener {
        fun onTodoEdited(todo: Todo)
    }
}